/* Your JS here. */
// navigation bar


$(document).ready(function () {
    var NodePos = [{ 'id': '#artists', 'v': $('#artists').offset().top }, { 'id': '#album', 'v': $('#album').offset().top }, { 'id': '#MV', 'v': $('#MV').offset().top }, { 'id': '#Whyus', 'v': $('#Whyus').offset().top + 80 }, { 'id': '#Aboutus', 'v': $('#Aboutus').offset().top + 90 }];
    $('.btn li').on("click", function (e) {
        var _this = $(this);
        var a = _this.children("a");
        var _href = a.attr("href");
        // a.addClass('active')
        var s = 0
        for (let index = 0; index < NodePos.length; index++) {
            const e = NodePos[index];
            if (e.id == _href) {
                s = e.v
            }
        }
        e.preventDefault();
        $("body").animate({
            'scrollTop': s - 69
        }, 1000);
    });
});


$(document.body).scroll(function (event) {
    checkscroll()
});

var NodePos = [$('#artists').offset().top, $('#album').offset().top, $('#MV').offset().top, $('#Whyus').offset().top, $('#Aboutus').offset().top];
function checkscroll() {
    var winPos = $(document.body).scrollTop() + 90; //屏幕位置
    length = NodePos.length;
    if (winPos <= NodePos[1]) {
        $('.btn li a').removeClass('active');
        $('.btn li:nth-child(1) a').addClass('active');
    } else {
        for (var i = 1; i < length; i++) {
            if ((i < length - 1 && winPos < NodePos[i + 1] && winPos >= NodePos[i]) || (i == length - 1 && winPos >= NodePos[i])) {
                $('.btn li a').removeClass('active');
                $('.btn li:nth-child(' + (i + 1) + ') a').addClass('active');
                break;
            }
        }
    }
}

$(document.body).scroll(function () {
    var scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
    if (scrollTop > 10) {
        $('.navigation-bar').addClass('heightChange')
    } else {
        $('.navigation-bar').removeClass('heightChange')
    }
});

// artisits part
// $(document).ready(function () {
//     var mySwiper = new Swiper('.swiper-container', {
//         autoplay: true,
//         loop: true,
//     })
// });

var swiper = new Swiper('.swiper-container', {
    speed: 600,
    parallax: true,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});